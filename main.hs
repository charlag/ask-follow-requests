import Web.Hastodon

main :: IO ()
main = do
  let cliendId = "FILL ME IN"
  let clientSecret = "FILL ME IN"
  let email = "FILL ME IN"
  let password = "FILL ME IN"
  let server = "fill.me"
  maybeClient <- mkHastodonClient cliendId clientSecret email password server
  case maybeClient of
    Just client -> do
      maybeRequests <- getFollowRequests client
      case maybeRequests of
        Left requestsError -> do
          putStrLn "Error"
          putStrLn $ show requestsError
        Right requests -> do
          _ <- sequence $ fmap (sendStatus client) requests
          return ()
    Nothing -> do
      putStrLn "Failed to log in"

-- VisibilityDirect
sendStatus :: HastodonClient -> Account -> IO ()
sendStatus client account =
  let username = "@" ++ (accountAcct account)
      option = visibility VisibilityDirect
      text = username ++ " hi! I ask people to @ me before following. Mind telling something about yourself? Thanks. (this is a semi-automated message)" in
    do
      response <- postStatusWithOption client option text 
      case response of
        Left _ -> putStrLn $ "Error writing to " ++ username
        Right _ -> putStrLn $ "Successfylly responded to " ++ username
